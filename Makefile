V=2

INSTALL_SRC_DIR=hyperiso/initcpio/install
HOOKS_SRC_DIR=hyperiso/initcpio/hooks
SCRIPT_SRC_DIR=hyperiso/initcpio/script

INSTALL_DIR=$(DESTDIR)/lib/initcpio/install
HOOKS_DIR=$(DESTDIR)/lib/initcpio/hooks
SCRIPT_DIR=$(DESTDIR)/lib/initcpio
DOC_DIR=$(DESTDIR)/usr/share/doc/hyperiso


all: install dist

install: install-program install-initcpio install-examples install-doc

install-program:
	# install to sbin since script only usable by root
	install -D -m 755 hyperiso/mkhyperiso "$(DESTDIR)/usr/sbin/mkhyperiso"

install-initcpio:
	install -d "$(SCRIPT_DIR)" "$(HOOKS_DIR)" "$(INSTALL_DIR)"
	install -m 755 -t "$(SCRIPT_DIR)" $(wildcard $(SCRIPT_SRC_DIR)/*)
	install -m 644 -t "$(HOOKS_DIR)" $(wildcard $(HOOKS_SRC_DIR)/*)
	install -m 644 -t "$(INSTALL_DIR)" $(wildcard $(INSTALL_SRC_DIR)/*)

install-examples:
	install -d -m 755 "$(DESTDIR)/usr/share/hyperiso/"
	cp -a --no-preserve=ownership configs "$(DESTDIR)/usr/share/hyperiso/"

install-doc:
	install -d "$(DOC_DIR)"
	install -m 644 -t "$(DOC_DIR)" $(wildcard docs/*)

uninstall: uninstall-program uninstall-initcpio uninstall-examples uninstall-doc

uninstall-program:
	rm "$(DESTDIR)/usr/sbin/mkhyperiso"

uninstall-initcpio:
	$(foreach file,$(wildcard $(SCRIPT_SRC_DIR)/*),rm -r "$(subst $(SCRIPT_SRC_DIR),$(SCRIPT_DIR),$(file))";)
	$(foreach file,$(wildcard $(HOOKS_SRC_DIR)/*),rm -r "$(subst $(HOOKS_SRC_DIR),$(HOOKS_DIR),$(file))";)
	$(foreach file,$(wildcard $(INSTALL_SRC_DIR)/*),rm -r "$(subst $(INSTALL_SRC_DIR),$(INSTALL_DIR),$(file))";)

uninstall-examples:
	rm -rfd "$(DESTDIR)/usr/share/hyperiso/configs"
	rm -d "$(DESTDIR)/usr/share/hyperiso"

uninstall-doc:
	rm -rf "$(DOC_DIR)"

dist:
	./expand $(V) dist

dist-branches:
	./expand $(V) dist-branches

.PHONY: install install-program install-initcpio install-examples install-doc dist dist-branches uninstall uninstall-program uninstall-initcpio uninstall-examples uninstall-doc
